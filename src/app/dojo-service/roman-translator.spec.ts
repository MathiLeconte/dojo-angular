import { TestBed } from "@angular/core/testing";
import {RomanTranslator} from "./roman-translator";

describe('RomanTranslator', () => {
    let romanTranslator: RomanTranslator;
    beforeEach(async () => {
        romanTranslator = new RomanTranslator();
      });

    it('should return one', () => {
        const entry = 1;
        romanTranslator.translate(entry);
        return expect(romanTranslator.translate(entry)).toEqual('1');
      });
})