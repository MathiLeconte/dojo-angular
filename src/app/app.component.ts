import { Component } from '@angular/core';
import { RomanTranslator } from './dojo-service/roman-translator';

@Component({
  selector: 'dojo-angular-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'dojo-angular';
  arabicNumber: number|undefined;
  romanNumber= '';
  romanTranslator = new RomanTranslator();

  translate(){
    console.log('traduit: ', this.arabicNumber);
    if(this.arabicNumber==undefined){
      this.romanNumber='';
    }else{
      this.romanNumber=this.romanTranslator.translate(this.arabicNumber);
    }
    console.log('resultat: ', this.romanNumber);
  }


}
